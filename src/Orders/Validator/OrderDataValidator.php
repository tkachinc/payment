<?php
/**
 * Created by PhpStorm.
 * User: gollariel
 * Date: 11/17/16
 * Time: 00:30
 */

namespace TkachInc\Payment\Orders\Validator;

use TkachInc\Engine\Services\Helpers\ArrayHelperException;
use TkachInc\Engine\Services\Validator\AbstractValidator;

class OrderDataValidator extends AbstractValidator
{
	public function initValidator()
	{
		$this->setRequiredFields([
			'firstName'    => '',
			'lastName'     => '',
			'dayOfBirth'   => '',
			'monthOfBirth' => '',
			'yearOfBirth'  => '',
			'email'        => '',
			'confirmEmail' => '',
			'amount'       => 0,
			'phone'        => 0,
		]);
	}

	public function fieldFirstName()
	{
		return function ($field, $value) {
			return $value;
		};
	}

	public function fieldLastName()
	{
		return function ($field, $value) {
			return $value;
		};
	}

	public function fieldDayOfBirth()
	{
		return function ($field, $value) {
			return $value;
		};
	}

	public function fieldMonthOfBirth()
	{
		return function ($field, $value) {
			return $value;
		};
	}

	public function fieldYearOfBirth()
	{
		return function ($field, $value) {
			return $value;
		};
	}

	public function fieldAmount()
	{
		return function ($field, $value) {
			return $value;
		};
	}

	public function fieldEmail()
	{
		return function ($key, $value) {
			if (!isset($value) || !filter_var(trim($value), FILTER_VALIDATE_EMAIL)) {
				throw new \Exception('Email field is not valid');
			}
			return $value;
		};
	}

	public function fieldConfirmEmail($data, $default)
	{
		return function ($key, $value)use($data) {
			if (!isset($value) || !filter_var(trim($value), FILTER_VALIDATE_EMAIL)) {
				throw new \Exception('Confirm email field is not valid');
			}
			if (!isset($data['email']) || $data['email'] !== $value) {
				throw new \Exception('Confirm email field is not valid');
			}
			return $value;
		};
	}

	public function fieldPhone()
	{
		return function ($field, $value) {
			$value = str_replace(['+', '-'], '', filter_var($value, FILTER_SANITIZE_NUMBER_INT));
			if (strlen($value) < 7) {
				throw new ArrayHelperException('Phone in not correct');
			}

			return $value;
		};
	}
}