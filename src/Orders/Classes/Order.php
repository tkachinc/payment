<?php

namespace TkachInc\Payment\Orders\Classes;

use TkachInc\Engine\Services\Helpers\Generator\HashGenerator;
use TkachInc\Payment\Orders\Model\OrderModel;

class Order
{
	protected $order;

	protected function __construct(OrderModel $order, $pk = null)
	{
		$this->order = $order;
		if (!$this->order->isLoadedObject()) {
			$this->order->_id = $pk;
			$this->order->humanId = HashGenerator::getSmallUniqueID();
			$this->order->time = time();
			$this->order->countPeople = 1;
		}
	}

	public function isLoaded()
	{
		return $this->order->isLoadedObject();
	}

	public function isComplete()
	{
		return $this->order->complete;
	}

	public function isSuccess()
	{
		return $this->order->success;
	}

	public static function init()
	{
		$id = HashGenerator::getStringUniqueId();
		$order = new OrderModel($id);

		return new Order($order, $id);
	}

	public static function get($orderId)
	{
		$order = new OrderModel($orderId);

		return new Order($order, $orderId);
	}

	public static function getByHumanId($humanId)
	{
		$id = HashGenerator::getStringUniqueId();
		$order = new OrderModel(['humanId' => $humanId]);

		return new Order($order, $id);
	}

	public function setData(array $data)
	{
		$this->order->setFieldsArray($data);

		return $this;
	}

	public function getParam($key, $default = null)
	{
		return $this->order->{$key}??$default;
	}

	public function toArray()
	{
		return $this->order->getFieldsArray();
	}


	public function save()
	{
		$this->order->save();

		return $this;
	}

	public function response($response)
	{
		$this->order->response = $response;

		return $this;
	}

	public function complete()
	{
		$this->order->complete = true;

		return $this;
	}

	public function success()
	{
		$this->order->success = true;

		return $this;
	}

	public function error($message)
	{
		$this->order->error = $message;

		return $this;
	}

	public function getId()
	{
		return $this->order->_id;
	}

	public function getProductId()
	{
		return $this->order->productId;
	}

	public function getProductGlobalId()
	{
		return $this->order->productGlobalId;
	}

	public function getHumanId()
	{
		return $this->order->humanId;
	}

	public function __get($name)
	{
		return $this->order->{$name};
	}
}