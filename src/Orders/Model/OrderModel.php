<?php
/**
 * Created by PhpStorm.
 * User: gollariel
 * Date: 11/16/16
 * Time: 01:17
 */

namespace TkachInc\Payment\Orders\Model;

use TkachInc\Core\Database\MongoDB\ObjectModel;

class OrderModel extends ObjectModel
{
	protected static $_writeConcernMode = 1;
	protected static $_collection = 'order_model';
	protected static $_pk = '_id';
	protected static $_sort = ['time' => -1];
	protected static $_indexes = [
		[
			'keys'   => ['_id' => 1],
			'unique' => true,
		],[
			'keys'   => ['humanId' => 1],
		],
	];

	protected static $_fieldsDefault = [
		'_id'      => '',
		'humanId'      => '',
		'userId' => '',
		'email' => '',
		'transactionId' => '',
		'firstName'    => '',
		'lastName'     => '',
		'dayOfBirth'   => '',
		'monthOfBirth' => '',
		'yearOfBirth'  => '',
		'confirmEmail' => '',
		'price'       => 0,
		'taxes'       => 0,
		'amount'       => 0,
		'type'       => 0,
		'currency'       => '',
		'currency_raw'       => '',
		'phone'        => '',
		'time'        => 0,
		'complete' => false,
		'success' => false,
		'booking' => false,
		'error' => '',
		'response' => [],
		'systemOrderId' => '',
		'status' => '',
		'productId' => '',
		'productGlobalId' => '',
		'date' => '',
		'locale' => '',
		'countPeople' => 1,
	];
	protected static $_fieldsValidate = [
		'_id'      => self::TYPE_STRING,
		'humanId'      => self::TYPE_STRING,
		'userId'      => self::TYPE_STRING,
		'email'      => self::TYPE_STRING,
		'transactionId' => self::TYPE_STRING,
		'firstName'    => self::TYPE_STRING,
		'lastName'     => self::TYPE_STRING,
		'dayOfBirth'   => self::TYPE_STRING,
		'monthOfBirth' => self::TYPE_STRING,
		'yearOfBirth'  => self::TYPE_STRING,
		'confirmEmail' => self::TYPE_STRING,
		'price'       => self::TYPE_DECIMAL,
		'taxes'       => self::TYPE_DECIMAL,
		'amount'       => self::TYPE_DECIMAL,
		'type'       => self::TYPE_STRING,
		'currency'       => self::TYPE_STRING,
		'currency_raw'       => self::TYPE_STRING,
		'phone'        => self::TYPE_STRING,
		'time'        => self::TYPE_TIMESTAMP,
		'complete' => self::TYPE_BOOL,
		'success' => self::TYPE_BOOL,
		'booking' => self::TYPE_BOOL,
		'error' => self::TYPE_STRING,
		'response' => self::TYPE_JSON,
		'systemOrderId' => self::TYPE_STRING,
		'status' => self::TYPE_STRING,
		'productId' => self::TYPE_STRING,
		'productGlobalId' => self::TYPE_STRING,
		'date' => self::TYPE_STRING,
		'locale' => self::TYPE_STRING,
		'countPeople' => self::TYPE_INT,
	];

	protected static $_fieldsPrivate = [
		'userId' => 1,
		'transactionId' => 1,
		'dayOfBirth'   => 1,
		'monthOfBirth' => 1,
		'yearOfBirth'  => 1,
		'confirmEmail' => 1,
		'type'       => 1,
		'time'        => 1,
		'complete' => 1,
		'success' => 1,
		'booking' => 1,
		'error' => 1,
		'response' => 1,
		'systemOrderId' => 1,
		'status' => 1,
		'productId' => 1,
		'productGlobalId' => 1,
	];

	// ОБЕЗАТЕЛЬНЫЕ ПОЛЯ
	protected static $_isCacheOn = true;
}