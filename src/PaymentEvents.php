<?php
namespace TkachInc\Payment;

use Evenement\EventEmitter;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class PaymentEvents extends EventEmitter
{
	const EVENT_PRODUCT_PAYMENT = 'event.product.payment';
	const EVENT_PRODUCT_INFO = 'event.product.info';

	protected $subscribers = [];

	/**
	 * @param          $event
	 * @param callable $listener
	 */
	public function subscribe($event, callable $listener)
	{
		$this->subscribers[$event] = $listener;
	}

	/**
	 * @param       $event
	 * @param array $arguments
	 * @return mixed
	 */
	public function call($event, array $arguments = [])
	{
		if (isset($this->subscribers[$event])) {
			$result = call_user_func_array($this->subscribers[$event], $arguments);
		} else {
			$result = null;
		}
		$this->emit($event, $arguments);

		return $result;
	}
}