<?php
namespace TkachInc\Payment;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
interface InterfacePaymentEvents
{
	/**
	 * @param ConfigParams $params
	 * @param              $serviceId
	 * @param              $netId
	 * @param              $socId
	 * @return mixed
	 */
	public function infoEvent(ConfigParams $params, $serviceId, $netId, $socId);

	/**
	 * @param ConfigParams $params
	 * @param              $serviceId
	 * @param              $netId
	 * @param              $socId
	 * @param              $transactionId
	 * @param              $options
	 * @return mixed
	 */
	public function paymentEvent(ConfigParams $params, $serviceId, $netId, $socId, $transactionId, $options);

	/**
	 * @param ConfigParams $params
	 * @param              $serviceId
	 * @param              $netId
	 * @param              $socId
	 * @param              $transactionId
	 * @param array $options
	 * @return mixed
	 */
	public function paymentOffer(ConfigParams $params,
	                             $serviceId,
	                             $netId,
	                             $socId,
	                             $transactionId,
	                             $options);
}