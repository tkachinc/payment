<?php
namespace TkachInc\Payment;

use TkachInc\Payment\PlatformsCallback\BaseCallback;
use TkachInc\Core\Log\FastLog;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class PaymentFactory
{
	/**
	 * @var ConfigParams
	 */
	protected $params;
	protected $paymentEvents;

	/**
	 * @param InterfacePaymentEvents $paymentEvents
	 * @param ConfigParams $params
	 */
	public function __construct(InterfacePaymentEvents $paymentEvents, ConfigParams $params)
	{
		$this->params = $params;
		$this->paymentEvents = $paymentEvents;
	}

	/**
	 * @param $netId
	 * @param $platform
	 * @return string
	 * @throws \Exception
	 */
	public function execute($netId, $platform)
	{
		$class = __NAMESPACE__ . '\\PlatformsCallback\\' . $platform . '\\PaymentCallback';
		if (!class_exists($class)) {
			throw new \Exception('Platform class not found');
		}

		try {
			/** @var BaseCallback $payment */
			$payment = new $class($this->paymentEvents, $this->params, $netId);
			if (!($payment instanceof BaseCallback)) {
				throw new \Exception('Platform must be instanceof to BaseCallback');
			}

			$response = $payment->execute();
		} catch (\Exception $e) {
			$response = $e->getMessage();
			FastLog::add(
				'payment_error',
				[
					'message' => $e->getMessage(),
					'code'    => $e->getCode(),
					'file'    => $e->getFile(),
					'line'    => $e->getLine(),
				]
			);
		}

		while (ob_get_level()) {
			ob_end_clean();
		}

		return $response;
	}
}