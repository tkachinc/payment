<?php
namespace TkachInc\Payment\Transaction;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class Transaction
{
	const TYPE_TRANSACTION_WITHDRAW = 'withdraw';
	const TYPE_TRANSACTION_DEPOSIT = 'deposit';

	/**
	 * @param TransactionId $id
	 *
	 * @return TransactionModel
	 */
	public static function getModel(TransactionId $id)
	{
		return new TransactionModel($id->get());
	}

	/**
	 * @param $userId
	 * @param $type
	 * @param $transactionId
	 *
	 * @return TransactionModel
	 */
	public static function getModelByUser($userId, $type, $transactionId)
	{
		return new TransactionModel(['id' => $userId, 'type' => $type, 'transactionId' => (string)$transactionId]);
	}

	/**
	 * @param TransactionId  $id
	 * @param                $userId
	 * @param                $type
	 * @param                $prices
	 * @param null|int       $transactionId
	 * @param array          $data
	 * @param array          $callback
	 *
	 * @throws TransactionException
	 * @return TransactionModel
	 */
	public static function create(TransactionId $id,
	                              $userId,
	                              $type,
	                              $prices,
	                              $transactionId = null,
	                              Array $data = [],
	                              Array $callback = [])
	{
		if ($transactionId === null || empty($transactionId)) {
			$transactionId = mt_rand(0, 10000) . time();
		}

		$model = new TransactionModel($id->get());
		if ($model->isLoadedObject() && $model->completed === true) {
			throw new TransactionException('Double transaction');
		} elseif ($model->isLoadedObject()) {
			return $model;
		}

		$model = new TransactionModel(['id' => $userId, 'type' => $type, 'transactionId' => (string)$transactionId]);
		if ($model->isLoadedObject() && $model->completed === true) {
			throw new TransactionException('Double transaction');
		} elseif ($model->isLoadedObject()) {
			return $model;
		}

		$model->_id = $id->get();
		$model->transactionId = (string)$transactionId;
		$model->id = $userId;
		$model->type = $type;
		$model->prices = $prices;
		$model->date = time();
		$model->data = $data;
		if (is_callable($callback)) {
			$model->callback = $callback;
		}
		$model->save();

		return $model;
	}

	/**
	 * @param TransactionId $id
	 * @param bool          $completed
	 * @param string        $comment
	 *
	 * @return mixed
	 */
	public static function completed(TransactionId $id, $completed = true, $comment = '')
	{
		$transaction = TransactionModel::findAndModify(
			['_id' => $id->get()],
			['$set' => ['completed' => $completed, 'comment' => $comment, 'end_date' => time()]]
		);
		if (isset($transaction['callback']) && is_callable($transaction['callback'])) {
			$completed = call_user_func_array($transaction['callback'], [$transaction]);
		}

		return $completed;
	}

	/**
	 * @param        $userId
	 * @param        $type
	 * @param        $transactionId
	 * @param bool   $completed
	 * @param string $comment
	 *
	 * @return mixed
	 */
	public static function completedByUser($userId, $type, $transactionId, $completed = true, $comment = '')
	{
		$transaction = TransactionModel::findAndModify(
			['id' => $userId, 'type' => $type, 'transactionId' => (string)$transactionId],
			['$set' => ['completed' => $completed, 'comment' => $comment, 'end_date' => time()]]
		);

		return $transaction;
	}

	/**
	 * @param TransactionId $id
	 *
	 * @throws TransactionException
	 * @return TransactionModel
	 */
	public static function getTransactionById(TransactionId $id)
	{
		$transactionModel = new TransactionModel($id->get());
		if (!$transactionModel->isLoadedObject()) {
			throw new TransactionException('Not found transaction');
		}

		return $transactionModel;
	}

	/**
	 * @param TransactionModel $model
	 *
	 * @return bool
	 */
	public static function checkTransactionCompleted(TransactionModel $model)
	{
		return ($model->completed === true);
	}

	/**
	 * @param TransactionModel $model
	 * @param                  $invoice
	 *
	 * @return bool
	 */
	public static function checkTransactionInvoice(TransactionModel $model, $invoice)
	{
		return ($model->transactionId === (string)$invoice);
	}

	/**
	 * @param array $params
	 *
	 * @return array
	 */
	public static function getUserTransaction(Array $params = [])
	{
		return TransactionModel::getAllAdvanced($params);
	}
} 