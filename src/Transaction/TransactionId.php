<?php
namespace TkachInc\Payment\Transaction;

use TkachInc\Core\Database\MongoDB\ObjectModel;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class TransactionId
{
	protected $id;

	/**
	 * @param null $id
	 */
	public function __construct($id = null)
	{
		$this->id = ObjectModel::getMongoId($id);
	}

	/**
	 * @return string
	 */
	public function get()
	{
		return $this->id;
	}

	/**
	 * @return string
	 */
	public function getString()
	{
		return (string)$this->id;
	}
} 