<?php
namespace TkachInc\Payment\Transaction;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class TransactionException extends \Exception
{
} 