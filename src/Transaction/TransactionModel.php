<?php
namespace TkachInc\Payment\Transaction;

use TkachInc\Core\Database\MongoDB\ObjectModel;

/**
 * @property mixed id
 * @property mixed type
 * @property mixed name
 * @property mixed transactionId
 * @property mixed completed
 * @property mixed date
 * @property mixed _id
 * @property mixed prices
 * @property mixed success
 * @property array data
 * @property mixed requests
 * @property callable callback
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class TransactionModel extends ObjectModel
{
	protected static $_collection = 'base_transaction_history';
	protected static $_pk = '_id';
	protected static $_sort = ['_id' => -1];
	protected static $_readPreference = ObjectModel::RP_PRIMARY_PREFERRED;
	protected static $_writeConcernMode = 1;
	protected static $_indexes = [
		[
			'keys'   => ['id' => 1, 'type' => 1, 'transactionId' => 1],
			'unique' => true,
		],
		[
			'keys' => ['date' => -1],
		],
		[
			'keys' => ['end_date' => -1],
		],
		[
			'keys' => ['type' => 1],
		],
	];
	protected static $_fieldsDefault = [
		'_id'           => '',
		'transactionId' => '',
		'id'            => '',
		'type'          => '',
		'date'          => 0,
		'end_date'      => 0,
		'completed'     => false,
		'callback'      => [],
		'data'          => [],
		'prices'          => [],
		'comment'       => '',
	];

	protected static $_fieldsValidate = [
		'_id'           => self::TYPE_MONGO_ID,
		'transactionId' => self::TYPE_STRING,
		'id'            => self::TYPE_STRING,
		'type'          => self::TYPE_STRING,
		'date'          => self::TYPE_TIMESTAMP,
		'end_date'      => self::TYPE_TIMESTAMP,
		'completed'     => self::TYPE_BOOL,
		'callback'      => self::TYPE_JSON,
		'data'          => self::TYPE_JSON,
		'prices'          => self::TYPE_JSON,
		'comment'       => self::TYPE_STRING,
	];

	// ОБЕЗАТЕЛЬНЫЕ ПОЛЯ
	protected static $_isCacheOn = true;
	protected static $_updateMethod = self::UPDATE_METHOD_SET;
} 