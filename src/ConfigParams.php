<?php
namespace TkachInc\Payment;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class ConfigParams
{
	protected $appId;
	protected $appSecretKey;

	/**
	 * @param $appSecretKey
	 * @param $appId
	 */
	public function __construct($appSecretKey, $appId)
	{
		$this->appSecretKey = $appSecretKey;
		$this->appId = $appId;
	}

	public function getAppId()
	{
		return $this->appId;
	}

	public function getAppSecretKey()
	{
		return $this->appSecretKey;
	}
}