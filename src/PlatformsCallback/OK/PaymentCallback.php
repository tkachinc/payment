<?php
namespace TkachInc\Payment\PlatformsCallback\OK;

use TkachInc\Payment\PlatformsCallback\CM\Helper\OKPaymentHelper;
use TkachInc\Payment\PlatformsCallback\BaseCallback;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class PaymentCallback extends BaseCallback
{
	public function execute()
	{
		$appSecret = $this->params->getAppSecretKey();

		$params = $_GET;
		if (!isset($_REQUEST['amount']) ||
			!isset($_REQUEST['product_code']) ||
			!isset($_REQUEST['uid']) ||
			!isset($_REQUEST['transaction_id'])
		) {
			throw new \Exception('Not found required params');
		}

		/* ВАЛИДАЦИЯ АУТЕНТИЧНОСТИ ЗАПРОСА */
		if (OKPaymentHelper::requestCheck($params, $appSecret)) {
			$price = (float)$_REQUEST['amount'];
			$serviceId = (int)$_REQUEST['product_code'];
			$userId = (string)$_REQUEST['uid'];
			$transactionId = (string)$_REQUEST['transaction_id'];
			$extraAttributes = (string)isset($_REQUEST['extra_attributes']) ? $_REQUEST['extra_attributes'] : '';

			try {
				$result = $this->paymentEvent(
					$serviceId,
					$userId,
					$transactionId,
					['price' => $price, 'additionalParams' => json_decode($extraAttributes, true)]
				);

				if ($result) {
					$responseCode = OKPaymentHelper::PAYMENT_SUCCESS;
					$responseMsg = '';
				} else {
					$responseCode = OKPaymentHelper::ERROR_CALLBACK_INVALID_PAYMENT;
					$responseMsg = '';
				}
			} catch (\Exception $e) {
				$responseCode = OKPaymentHelper::ERROR_CALLBACK_INVALID_PAYMENT;
				$responseMsg = '';
			}
		} else {
			$responseCode = OKPaymentHelper::ERROR_PARAM_SIGNATURE;
			$responseMsg = 'Invalid signature';
		}

		while (ob_get_level()) {
			ob_end_clean();
		}
		if ($responseCode == OKPaymentHelper::PAYMENT_SUCCESS) {
			OKPaymentHelper::responseSuccess();
		} else {
			OKPaymentHelper::responseError($responseCode, $responseMsg);
		}
	}
}