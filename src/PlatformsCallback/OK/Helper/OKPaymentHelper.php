<?php
namespace TkachInc\Payment\PlatformsCallback\CM\Helper;

/**
 * Class OKPaymentHelper
 *
 * @package TkachInc\Payment\Platform\OK\Helper
 */
class OKPaymentHelper
{
	const ERROR_UNKNOWN = 1;
	const ERROR_SERVICE = 2;
	const ERROR_CALLBACK_INVALID_PAYMENT = 1001;
	const ERROR_SYSTEM = 9999;
	const ERROR_PARAM_SIGNATURE = 104;
	const PAYMENT_SUCCESS = 0;

	public static function responseSuccess()
	{
		$xml = new \DomDocument('1.0', 'UTF-8');
		$cpr = $xml->createElement('callbacks_payment_response', 'true');
		$cprXmlns = $xml->createAttribute('xmlns');
		$cprXmlns->value = 'http://api.forticom.com/1.0/';
		$cpr->appendChild($cprXmlns);
		$xml->appendChild($cpr);
		$xmlOutput = $xml->saveXML();

		header('Content-Type: application/xml; charset=utf-8');
		echo $xmlOutput;
	}

	/**
	 * @param $code
	 * @param $msg
	 */
	public static function responseError($code, $msg)
	{
		$xml = new \DomDocument('1.0', 'UTF-8');
		$errorNode = $xml->createElement('ns2:error_response');
		$xmlns = $xml->createAttribute('xmlns:ns2');
		$xmlns->value = 'http://api.forticom.com/1.0/';
		$errorNode->appendChild($xmlns);
		$errorCodeNode = $xml->createElement('error_code', $code);
		$errorMsgNode = $xml->createElement('error_msg', $msg);
		$errorNode->appendChild($errorCodeNode);
		$errorNode->appendChild($errorMsgNode);
		$xml->appendChild($errorNode);
		$xmlOutput = $xml->saveXML();

		header('Content-Type: application/xml; charset=utf-8');
		header('invocation-error: ' . $code);
		echo $xmlOutput;
	}

	/**
	 * @param $userId
	 * @param $sessionKey
	 * @param $appSecretKey
	 * @return string
	 */
	public static function authSigGet($userId, $sessionKey, $appSecretKey)
	{
		return md5(
			$userId . $sessionKey . $appSecretKey
		);
	}

	/**
	 * @param $array
	 * @param $appSecretKey
	 * @return bool
	 */
	public static function requestCheck($array, $appSecretKey)
	{
		if (isset($array['logged_user_id'], $array['session_key'], $array['auth_sig'])) {
			return $array['auth_sig'] ==
			static::authSigGet($array['logged_user_id'], $array['session_key'], $appSecretKey) ? true : false;
		} else {
			$sig = isset($array['sig']) ? (string)$array['sig'] : '';
			unset($array['net_id'], $array['country'], $array['city'], $array['sig']);
			ksort($array);

			return ($sig == md5(
				urldecode(http_build_query($array, '', '')) . $appSecretKey
			) ? true : false);
		}
	}
}