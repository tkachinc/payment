<?php
namespace TkachInc\Payment\PlatformsCallback\VK;

use TkachInc\Payment\PlatformsCallback\BaseCallback;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class PaymentCallback extends BaseCallback
{
	/**
	 * @return array|mixed
	 */
	public function execute()
	{
		$response = [];
		$secret_key = $this->params->getAppSecretKey(); // Защищенный ключ приложения

		$input = $_POST;

		if (!isset($input['sig'])) {
			$response['error'] = [
				'error_code' => 10,
				'error_msg'  => 'Несовпадение вычисленной и переданной подписи запроса.',
				'critical'   => true,
			];

			return $response;
		}

		// Проверка подписи
		$sig = $input['sig'];
		unset($input['sig']);
		ksort($input);
		$str = urldecode(http_build_query($input, '', ''));

		/* ВАЛИДАЦИЯ АУТЕНТИЧНОСТИ ЗАПРОСА */
		if ($sig != md5($str . $secret_key)) {
			$response['error'] = [
				'error_code' => 10,
				'error_msg'  => 'Несовпадение вычисленной и переданной подписи запроса.',
				'critical'   => true,
			];

			return $response;
		}

		if (strpos($input['item'], 'offer') !== false) {
			$offerId = (int)mb_substr($input['item'], 6);
		} else {
			$offerId = null;
		}

		$serviceId = (int)$input['item']; // ID товара ВК
		$transactionId = (string)$input['order_id'];

		// Подпись правильная
		if (isset($offerId)) {
			$this->processOffer($input, $offerId, $transactionId);
		} elseif ($input['notification_type'] == 'get_item' || $input['notification_type'] == 'get_item_test') {
			return $this->getInfo($input, $serviceId, $transactionId);
		} elseif ($input['notification_type'] == 'order_status_change' ||
			$input['notification_type'] == 'order_status_change_test'
		) {
			return $this->processPayment($input, $serviceId, $transactionId);
		}

		// default case
		$response['error'] = [
			'error_code' => 100,
			'error_msg'  => 'Передано непонятно что вместо chargeable. Сумма списания не соответствует стомости покупки.',
			'critical'   => true,
		];

		return $response;
	}

	/**
	 * @param $input
	 * @param $serviceId
	 * @param $transactionId
	 * @return mixed
	 */
	protected function getInfo($input, $serviceId, $transactionId)
	{
		$socId = $input['user_id'];
		$description = $this->infoEvent($serviceId, $socId);

		if (!$description) {
			$response['error'] = [
				'error_code' => 20,
				'error_msg'  => 'Описания товара не существует.',
				'critical'   => true,
			];

			return $response;
		}

		$response['response'] = [
			'item_id'   => $serviceId,
			'title'     => $description['title'],
			'photo_url' => $description['image_url'],
			'price'     => $description['price'],
		];

		if (isset($description['expiration']) &&
			$description['expiration'] >= 600 &&
			$description['expiration'] <= 604800
		) {
			$response['response']['expiration'] = $description['expiration'];
		}

		return $response;
	}

	/**
	 * @param $input
	 * @param $serviceId
	 * @param $transactionId
	 * @return mixed
	 */
	protected function processPayment($input, $serviceId, $transactionId)
	{
		$socId = $input['user_id'];
		if ($input['status'] != 'chargeable') {
			$response['error'] = [
				'error_code' => 100,
				'error_msg'  => 'Неверный статус заказа.',
				'critical'   => true,
			];

			return $response;
		}

		$options = [];
		if (isset($input['item_price'])) {
			$options = ['price' => (int)$input['item_price']];
		}
		$result = $this->paymentEvent($serviceId, $socId, $transactionId, $options);

		if ($result) {
			$response['response'] = [
				'order_id'     => $transactionId,
				'app_order_id' => $transactionId, // Получающийся у вас идентификатор заказа.
			];

			return $response;
		}

		$response['error'] = [
			'error_code' => 100,
			'error_msg'  => 'Покупка уже недоступна игроку и не может быть обналичена.',
			'critical'   => true,
		];

		return $response;
	}

	/**
	 * @param $input
	 * @param $serviceId
	 * @param $transactionId
	 * @return mixed
	 */
	protected function processOffer($input, $serviceId, $transactionId)
	{
		$socId = $input['user_id'];
		if ($input['status'] != 'chargeable') {
			$response['error'] = [
				'error_code' => 100,
				'error_msg'  => 'Неверный статус заказа.',
				'critical'   => true,
			];

			return $response;
		}

		$options = [];
		if (isset($input['item_price'])) {
			$options = [
				'price'  => (int)$input['item_price'],
				'amount' => (int)isset($input['item_currency_amount']) ? $input['item_currency_amount'] : 0,
			];
		}
		$result = $this->paymentOffer($serviceId, $socId, $transactionId, $options);

		if ($result) {
			$response['response'] = [
				'order_id'     => $transactionId,
				'app_order_id' => $transactionId, // Получающийся у вас идентификатор заказа.
			];

			return $response;
		}

		$response['error'] = [
			'error_code' => 100,
			'error_msg'  => 'Покупка уже недоступна игроку и не может быть обналичена.',
			'critical'   => true,
		];

		return $response;
	}
}