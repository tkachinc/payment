<?php
namespace TkachInc\Payment\PlatformsCallback\FBRealTimeUpdates;

use Facebook\Facebook;
use TkachInc\Payment\PlatformsCallback\BaseCallback;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class PaymentCallback extends BaseCallback
{
	protected $verifyToken = 'bRffSID';

	public function execute()
	{
		$appId = $this->params->getAppId();
		$appSecret = $this->params->getAppSecretKey();

		if (isset($_REQUEST['hub_verify_token'], $_REQUEST['hub_challenge'])) {
			if ($_REQUEST['hub_verify_token'] === $this->verifyToken) {
				echo $_REQUEST['hub_challenge'];
				exit();
			}
		} else {
			$updates = json_decode(file_get_contents('php://input'), true);
			if (empty($updates)) {
				throw new \Exception('Updates is empty');
			}

			if ($updates['object'] == 'payments') {
				// могут прийти пачкой
				foreach ($updates['entry'] as $entry) {
					$this->pay($entry['id'], $appId, $appSecret);
				}
			}
		}
	}

	/**
	 * @param $paymentId
	 * @param $appId
	 * @param $appSecret
	 * @return bool|mixed
	 * @throws \Exception
	 */
	protected function pay($paymentId, $appId, $appSecret)
	{
		$result = false;

		$fb = new Facebook(
			[
				'app_id'                => $appId,
				'app_secret'            => $appSecret,
				'default_graph_version' => 'v2.2',
			]
		);
		$response = $fb->get(
			'/' .
			$paymentId .
			'?fields=id,user,application,actions,refundable_amount,items,country,request_id,created_time,payout_foreign_exchange_rate,tax,tax_country',
			$appId . '|' . $appSecret
		);
		//FastLog::add('DEBUG', [$paymentId, $response->getDecodedBody(), $response->getBody()]);
		$response = $response->getDecodedBody();

		if (empty($response)) {
			throw new \Exception('res: ' . json_encode($response));
		}

		$options = [];

		// автопроставление isTest для тестовых оплат
		if (!empty($response['test'])) {
			$options['isTest'] = true;
		}

		// handle disputes
		if (isset($response['disputes'])) {
			foreach ($response['disputes'] as $dispute) {
				// skip resolved disputes
				if ($dispute['status'] !== 'pending') {
					continue;
				}
				//$request = new FacebookRequest(
				//	$session, 'POST', '/'.$paymentId.'/dispute', [
				//		'access_token' => $appId.'|'.$appSecret,
				//		'reason'       => 'GRANTED_REPLACEMENT_ITEM'
				//	]
				//);
				//$disputeResponse = $request->execute()->getResponse();
			}

			// seems like we do not need to proceed futher
			return false;
		}
		foreach ($response['actions'] as $action) {
			if ($action['type'] != 'charge' || $action['status'] != 'completed') {
				continue;
			}

			// локальные валюты
			$options['local_amount'] = $action['amount'];
			$options['local_currency'] = $action['currency'];

			// пересчитываем цену и подменяем её в истории оплат
			$options['payment_amount'] = (float)$action['amount'] * $response['payout_foreign_exchange_rate'];

			// должен быть только один товар, но по спецификации fb может быть больше.
			$item = $response['items'][0];

			// вычисляем service_id товара по url-у og:product объекта
			$query = parse_url($item['product'], PHP_URL_QUERY);
			$queryGET = [];
			parse_str($query, $queryGET);
			if (!isset($queryGET['service_id'])) {
				continue;
			}

			$result = $this->paymentEvent($queryGET['service_id'], $response['user']['id'], $paymentId, $options);
		}

		return $result;

	}
}