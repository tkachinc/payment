<?php
namespace TkachInc\Payment\PlatformsCallback\MM;

use TkachInc\Payment\PlatformsCallback\BaseCallback;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class PaymentCallback extends BaseCallback
{
	/**
	 * @param $params
	 * @param $appSecretKey
	 * @return bool|string
	 */
	public static function checkSig($params, $appSecretKey)
	{
		if (isset($params['sig'])) {
			unset($params['sig']);
			unset($params['net_id']);
			ksort($params);
			$sigStr = '';
			foreach ($params as $paramName => $paramVal) {
				$sigStr .= $paramName . '=' . $paramVal;
			}
			$sigStr .= $appSecretKey;

			return md5($sigStr);
		} else {
			return false;
		}
	}

	/**
	 * @return array
	 */
	public function execute()
	{
		$appSecretKey = $this->params->getAppSecretKey();

		$params = $_GET;
		$sig = (string)$params['sig'];
		$validSig = (string)static::checkSig($params, $appSecretKey);
		$response = ['status' => 2, 'error_code' => 701];

		if ($validSig === $sig) /* ВАЛИДАЦИЯ АУТЕНТИЧНОСТИ ПЛАТЕЖА */ {
			$price = (float)$_REQUEST['mailiki_price'];
			$serviceId = (int)$_REQUEST['service_id'];
			$userId = (string)$_REQUEST['uid'];
			$transactionId = (string)$_REQUEST['transaction_id'];

			try {
				$result = $this->paymentEvent($serviceId, $userId, $transactionId, ['price' => $price]);

				if ($result) {
					return ['status' => 1];
				} else {
					return $response;
				}
			} catch (\Exception $e) {
				return $response;
			}
		} else {
			return $response;
		}
	}
}