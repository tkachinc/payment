<?php
namespace TkachInc\Payment\PlatformsCallback;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
abstract class BaseCallback
{
	use PayTrait;

	abstract public function execute();
}