<?php
namespace TkachInc\Payment\PlatformsCallback\GooglePlay\Service;

use TkachInc\Core\Log\FastLog;
use TkachInc\Payment\PlatformsCallback\BaseService;

/**
 * Верификация платежей в GooglePlay
 *
 * @package TkachInc\Payment\Platform\GooglePlay\Service
 */
class Manager extends BaseService
{
	/**
	 * Верификация order id
	 *
	 * @param string $productId - id товара
	 * @param string $purchaseToken - токен покупки, который вернул GooglePlay
	 * @param        $googleplay_purchase_status_api
	 * @return array
	 */
	public static function verify($productId, $purchaseToken, $googleplay_purchase_status_api)
	{
		$conf = $googleplay_purchase_status_api;

		$result = [
			'verified'  => false,
			'response'  => [],
			'isCheater' => false,
		];

		if (!empty($conf) && !empty($conf['enabled'])) {
			for ($k = 0; $k < $conf['retry_count']; $k++) {
				try {
					$client = new \Google_Client();
					$client->setApplicationName($conf['app_package_name']);
					$client->setAccessType('offline');
					$client->setClientId($conf['client_id']);
					$client->setClientSecret($conf['client_secret']);

					if (!empty($conf['curl_options'])) {
						$client->getIo()->setOptions($conf['curl_options']);
					}

					$client->getAuth()->refreshToken($conf['refresh_token']);

					$service = new \Google_Service_AndroidPublisher($client);
					for ($i = 0; $i < $conf['retry_count']; $i++) {
						try {
							$res = $service->inapppurchases->get(
								$conf['app_package_name'],
								$productId, // в документации $app_package_name . '.' . $productId, но так не работает
								$purchaseToken
							);

							// Верификация прошла успешно, выходим из цикла и переходим к зачислению
							$result['verified'] = true;
							$result['response'] = $res;

							return $result;
						} catch (\Exception $e) {
							FastLog::add(
								'payment_error',
								[
									'msg' => 'Try: ' .
										$i .
										' Exception: ' .
										$e->getMessage() .
										' on ' .
										$e->getFile() .
										':' .
										$e->getLine(),
								]
							);

							if ($e->getCode() == 400) {
								// Неверные productId или purchaseToken - читер
								$result['isCheater'] = true;

								return $result;
							}
							// В случае если у Гугла 503 ошибка (такое периодически бывает),
							// либо если access_token или ещё что не работает, то после всех попыток, проводим платёж без верификации
							// Задержка перед следующей попыткой
							sleep(1);
						}
					}

					return $result;

				} catch (\Exception $e) {
					FastLog::add(
						'payment_error',
						[
							'msg' => 'Exception while getting access token: ' .
								$e->getMessage() .
								' on ' .
								$e->getFile() .
								':' .
								$e->getLine(),
						]
					);
					// произошла какая-то ошибка с апи гугла, игнорим, проводим покупку без верификации
				}
			}
		}

		return $result;
	}

	/**
	 * Верификация order id
	 *
	 * @param string $googleOrderNumber - токен, полученный с маркета
	 * @param string $userId - id пользователя в проекте
	 * @param string $productId - id товара
	 * @param string $purchaseToken - токен покупки, который вернул GooglePlay
	 * @param        $googleplay_purchase_status_api
	 * @param array $options - дополнительные параметры
	 * @return array
	 */
	public function verifyPurchase(...$params)
	{
		list($googleOrderNumber, $userId, $productId, $purchaseToken, $googleplay_purchase_status_api, $options) = $params;

		$data = ['result' => false];
		$result = self::verify($productId, $purchaseToken, $googleplay_purchase_status_api);
		//die("Verified: ".(int)$verified);

		if ($result['isCheater']) {
			$data['payment_status'] = isset($result['errmsg']) ? $result['errmsg'] : 'Cheater!';

			return $data;
		}

		try {
			$result = $this->paymentEvent($productId, $userId, $googleOrderNumber, $options);

			if ($result) {
				$data['result'] = true;
				$data['payment_status'] = 'complete';
				$data['verified'] = $result['verified'];
			} else {
				FastLog::add(
					'payment_error',
					['msg' => 'GooglePlay payment error: user_id: ' . $userId . ' | googleOrderNumber: ' . $googleOrderNumber]
				);
				$data['payment_status'] = 'Payment event was not processed';
			}

		} catch (\Exception $e) {
			FastLog::add(
				'payment_error',
				['msg' => 'Exception: ' . $e->getMessage() . ' on ' . $e->getFile() . ':' . $e->getLine()]
			);
			$data['payment_status'] = 'Payment processing failed';
		}

		return $data;
	}
}