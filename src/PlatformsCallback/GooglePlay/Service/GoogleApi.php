<?php
namespace TkachInc\Payment\PlatformsCallback\GooglePlay\Service;

/**
 * Класс API с фейсбуком
 */
class GoogleApi
{
	/**
	 * @var string URL schema
	 */
	public $schema_url = "http://checkout.google.com/schema/2";

	/**
	 * @var string URL отчета
	 */
	public $prod_base_server_url = "https://checkout.google.com/api/checkout/v2/reports/Merchant/";

	/**
	 * @var string URL sandbox
	 */
	public $sandbox_base_server_url = "https://sandbox.google.com/checkout/api/checkout/v2/reports/Merchant/";

	/**
	 * @var string ID продавца
	 */
	public $merchant_id = '';

	/**
	 * @var string Ключ продавцы
	 */
	public $merchant_key = '';

	/**
	 * @var string Тип сервера
	 */
	public $server_type = '';

	/**
	 * @var string URL сервера
	 */
	public $server_url = '';

	/**
	 * @var string Базовый URL
	 */
	public $base_url = '';

	/**
	 * @var string  URL запросов
	 */
	public $request_url = '';

	/**
	 * @var string URL отчета
	 */
	public $report_url = '';

	/**
	 * @var string URL проверки заказа
	 */
	public $merchant_checkout = '';

	/**
	 * Готовит к использованию данные
	 */
	public function __construct($merchant_id, $merchant_key, $server_type)
	{
		$this->merchant_id = $merchant_id;
		$this->merchant_key = $merchant_key;

		$this->server_type = $server_type;

		if (strtolower($this->server_type) == "sandbox") {
			$this->server_url = "https://sandbox.google.com/checkout/";
		} else {
			$this->server_url = "https://checkout.google.com/";
		}

		$this->base_url = $this->server_url . "api/checkout/v2/";
		$this->request_url = $this->base_url . "request/Merchant/" . $this->merchant_id;
		$this->report_url = $this->base_url . "reports/Merchant/" . $this->merchant_id;
		$this->merchant_checkout = $this->base_url . "merchantCheckout/Merchant/" . $this->merchant_id;
	}

	/**
	 * Готовит заголовок авторизации
	 *
	 * @access public
	 */
	public function getAuthenticationHeaders()
	{
		$headers = [];
		$headers[] = "Authorization: Basic " . base64_encode($this->merchant_id . ':' . $this->merchant_key);
		$headers[] = "Content-Type: application/xml; charset=UTF-8";
		$headers[] = "Accept: application/xml; charset=UTF-8";
		$headers[] = "User-Agent: GC-PHP-Sample_code (v1.3.0/ropu)";

		return $headers;
	}

	/**
	 * Производит отправку запроса
	 *
	 * @access public
	 */
	public function sendReq($url, $header_arr, $post_args)
	{
		// Get the curl session object
		$session = curl_init();

		// Set the POST options.
		curl_setopt($session, CURLOPT_URL, $url);
		//curl_setopt( $session, CURLOPT_POST, 1 );
		curl_setopt($session, CURLOPT_HTTPHEADER, $header_arr);
		curl_setopt($session, CURLOPT_POSTFIELDS, $post_args);
		curl_setopt($session, CURLOPT_HEADER, 0);
		curl_setopt($session, CURLOPT_RETURNTRANSFER, 1);
		//curl_setopt($session, CURLOPT_FAILONERROR, 1);

		// FIXME Добавлено на время теста, для обращения к несертифицированому серверу
		curl_setopt($session, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($session, CURLOPT_SSL_VERIFYPEER, 0);

		// Do the POST and then close the session
		$response = curl_exec($session);
		if (curl_errno($session)) {
			return ["CURL_ERR", curl_error($session)];
		} else {
			curl_close($session);
		}

		return $response;
	}
}