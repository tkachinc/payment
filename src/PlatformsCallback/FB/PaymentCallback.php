<?php
namespace TkachInc\Payment\PlatformsCallback\FB;

use TkachInc\Payment\PlatformsCallback\BaseCallback;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class PaymentCallback extends BaseCallback
{
	/**
	 * @return array
	 * @throws \Exception
	 */
	public function execute()
	{
		$appId = $this->params->getAppId();
		$appSecret = $this->params->getAppSecretKey();

		$request = null;
		if (isset($_GET['signed_request'])) {
			$request = $_GET['signed_request'];
		} elseif (isset($_COOKIE['fbsr_' . $appId])) {
			$request = $_COOKIE['fbsr_' . $appSecret];
		} elseif (isset($_POST['signed_request'])) {
			$request = $_POST['signed_request'];
		}

		// Prepare the return data array
		$data = ['content' => []];

		if ($request == null) {
			// Handle an unauthenticated request here
			echo "Wrong request\n";
			exit();
		}

		if (!isset($_REQUEST['method'])) {
			throw new \Exception('Not found method');
		}

		// Retrieve all params passed in
		$method = $_REQUEST['method'];

		// Подпись правильная
		if ($method == 'payments_get_items') {
			// Grab the payload
			$payload = $request['credits'];
			//		$order_id = $payload['order_id'];

			// remove escape characters
			$order_info = stripcslashes($payload['order_info']);
			$item_info = json_decode($order_info, true);
			$serviceId = (int)$item_info;

			$description = $this->infoEvent($serviceId, $request['user_id']);

			$data['content'] = $description;
		} elseif ($method == 'payments_get_item_price') {
			// Grab the payload
			// Retrieve all params passed in
			$payload = $request['payment'];
			$productUrl = $payload['product'];

			// вычисляем service_id товара по url-у og:product объекта
			$query = parse_url($productUrl, PHP_URL_QUERY);
			$queryParams = [];
			parse_str($query, $queryParams);
			if (!isset($queryParams['serviceId'])) {
				throw new \Exception('serviceId not found');
			}
			$serviceId = $queryParams['serviceId'];

			$description = $this->infoEvent($serviceId, $request['user_id']);

			$data['content'] = $description;
		} elseif ($method == 'payments_status_update') {
			// Grab the payload
			$payload = $request['credits'];
			$order_id = $payload['order_id'];

			// Grab the order status
			$status = $payload['status'];

			//trigger_error($func . " " . json_encode($status) . "\n" . print_r($request, 1));
			// Write your apps logic here for validating and recording a
			// purchase here.
			//
			// Generally you will want to move states from `placed` -> `settled`
			// here, then grant the purchasing user's in-game item to them.
			if ($status == 'placed') {
				$order_details = (array)json_decode($payload['order_details']);
				$socId = (string)$order_details['buyer'];
				$transactionId = $order_details['order_id'];
				$item = (array)$order_details['items'][0];
				$serviceId = (int)$item['item_id'];

				$result = $this->paymentEvent($serviceId, $socId, $transactionId, []);

				if ($result) {
					$next_state = 'settled';
				} else {
					$next_state = 'canceled';
				}

				$data['content']['status'] = $next_state;
			}

			$data['content']['order_id'] = $order_id;
		}

		// Required by api_fetch_response()
		$data['method'] = $method;

		return $data;
	}
}