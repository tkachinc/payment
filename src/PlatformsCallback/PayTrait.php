<?php
namespace TkachInc\Payment\PlatformsCallback;

use TkachInc\Payment\ConfigParams;
use TkachInc\Payment\InterfacePaymentEvents;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
trait PayTrait
{
	protected $payEvent, $params, $netId;

	/**
	 * @param InterfacePaymentEvents $paymentEvents
	 * @param ConfigParams $params
	 * @param                        $netId
	 */
	public function __construct(InterfacePaymentEvents $paymentEvents, ConfigParams $params, $netId)
	{
		$this->params = $params;
		$this->netId = $netId;
		$this->paymentEvents = $paymentEvents;
	}

	/**
	 * @param $serviceId
	 * @param $socId
	 * @return mixed
	 */
	public function infoEvent($serviceId, $socId)
	{
		return $this->paymentEvents->infoEvent($this->params, $serviceId, $this->netId, $socId);
	}

	/**
	 * @param            $serviceId
	 * @param            $socId
	 * @param            $transactionId
	 * @param array $options
	 * @return mixed
	 */
	public function paymentEvent($serviceId, $socId, $transactionId, Array $options = [])
	{
		return $this->paymentEvents->paymentEvent(
			$this->params,
			$serviceId,
			$this->netId,
			$socId,
			$transactionId,
			$options
		);
	}

	/**
	 * @param $serviceId
	 * @param $socId
	 * @param $transactionId
	 * @param $options
	 * @return mixed
	 */
	public function paymentOffer($serviceId, $socId, $transactionId, $options)
	{
		return $this->paymentEvents->paymentOffer(
			$this->params,
			$serviceId,
			$this->netId,
			$socId,
			$transactionId,
			$options
		);
	}
}