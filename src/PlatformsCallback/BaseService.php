<?php
namespace TkachInc\Payment\PlatformsCallback;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
abstract class BaseService
{
	use PayTrait;

	abstract public function verifyPurchase(...$params);
}