<?php
namespace TkachInc\Payment\PlatformsCallback\AppStore\Service;

use TkachInc\Core\Log\FastLog;
use TkachInc\Payment\PlatformsCallback\AppStore\Model\Transaction;
use TkachInc\Payment\PlatformsCallback\BaseService;

/**
 * Class iTunes
 *
 * @package TkachInc\Payment\Platform\iOS\Service
 */
class ITunes extends BaseService
{
	/**
	 * Верификация покупки на iTunes
	 *
	 * @param string $receipt
	 * @param string $userId
	 * @param        $itunes_bundel_id
	 * @param string $transactionId
	 * @param array $options - дополнительные параметры
	 * @return array
	 */
	public function verifyPurchase(...$params)
	{
		list($receipt, $userId, $itunes_bundel_id, $transactionId, $options) = $params;
		$data['result'] = false;

		if (empty($receipt)) {
			FastLog::add('payment_error', ['msg' => "Api iTunesVerifyPurchase. Receipt not found"]);
			$data['payment_status'] = 'Receipt not found';

			return $data;
		}

		// Если нам передали $transactionId, то мы можем осуществить быструю проверку существования транзакции.
		if (!empty($transactionId)) {
			// Check duplicate Transaction ID
			$countTransaction = Transaction::count(['transaction_id' => $transactionId]);
			if ($countTransaction > 0) {
				FastLog::add('payment_error', ['msg' => "Api iTunesVerifyPurchase. Duplicate Transaction"]);
				$data['payment_status'] = 'Duplicate Transaction';

				return $data;
			}
		}

		// verify the receipt
		$results = self::getReceiptData($receipt);
		foreach ($results as $result) {
			// Transaction ID is blank
			if (!isset($result['transaction_id']) || empty($result['transaction_id'])) {
				FastLog::add('payment_error', ['msg' => "Api iTunesVerifyPurchase. Transaction ID is blank"]);
				$data['payment_status'] = 'Transaction ID is blank';

				return $data;
			}

			// Проверка транзакции после получения данных от iTunes
			if (empty($transactionId) || ($transactionId != $result['transaction_id'])) {
				// Check duplicate Transaction ID
				$countTransaction = Transaction::count(['transaction_id' => $result['transaction_id']]);
				if ($countTransaction > 0) {
					FastLog::add('payment_error', ['msg' => "Api iTunesVerifyPurchase. Duplicate Transaction"]);
					$data['payment_status'] = 'Duplicate Transaction';

					return $data;
				}
			}

			$bundle_id = $itunes_bundel_id;
			if (($result['bid'] == 'com.zeptolab.ctrexperiments') // очень популярный вариант
				|| (!empty($bundle_id) && $bundle_id != $result['bid'])
			) {
				FastLog::add('payment_error', ['msg' => "Api iTunesVerifyPurchase. Wrong bundle id"]);
				$data['payment_status'] = 'Wrong bundle id';

				return $data;
			}

			if (!isset($result['fail']) &&
				$result['status'] == 0 &&
				isset($result['product_id']) &&
				!empty($result['product_id'])
			) {
				try {
					$result = $this->paymentEvent($result['product_id'], $userId, $result['transaction_id'], $options);
					if ($result) {
						// TODO: перенести в ивент?
						$transaction = new Transaction();
						$transaction->user_id = $userId;
						$transaction->transaction_id = $result['transaction_id'];
						$transaction->time = time();
						$transaction->endpoint = $result['endpoint'];
						$transaction->itunes_response = $result;
						if ($transaction->save()) {
							$data['transaction_status'] = 'transaction saved';
						} else {
							FastLog::add(
								'payment_error',
								[
									'msg' => 'iTunes Transaction not saved ' .
										(isset($result['errmsg']) ? ' | errmsg: ' . $result['errmsg'] : '') .
										(isset($result['status']) ? ' | itunes status: ' . $result['status'] : '') .
										(isset($result['transaction_id']) ? ' | itunes transaction_id: ' .
											$result['transaction_id'] : ''),
								]
							);
							$data['transaction_status'] = 'transaction is not saved';
						}
						$data['result'] = true;
						$data['payment_status'] = 'complete';
					} else {
						FastLog::add(
							'payment_error',
							[
								'msg' => 'iTunes payment error: user_id: ' .
									$userId .
									' ' .
									(isset($result['transaction_id']) ? ' | itunes transaction_id: ' .
										$result['transaction_id'] : ''),
							]
						);
						$data['payment_status'] = isset($result['errmsg']) ? $result['errmsg'] : 'Payment event was not processed';
					}

				} catch (\Exception $e) {
					FastLog::add(
						'payment_error',
						[
							'msg' => 'iTunes payment error: user_id: ' .
								$userId .
								' ' .
								(isset($result['transaction_id']) ? ' | itunes transaction_id: ' .
									$result['transaction_id'] : '') .
								'Exception: ' .
								$e->getMessage() .
								' on ' .
								$e->getFile() .
								':' .
								$e->getLine(),
						]
					);
					$data['payment_status'] = isset($result['errmsg']) ? $result['errmsg'] : 'Payment processing failed';
				}
			} else {
				FastLog::add(
					'payment_error',
					[
						'msg' => 'iTunes payment error: user_id: ' .
							$userId .
							' ' .
							(isset($result['errmsg']) ? ' | errmsg: ' . $result['errmsg'] : '') .
							(isset($result['status']) ? ' | itunes status: ' . $result['status'] : '') .
							(isset($result['transaction_id']) ? ' | itunes transaction_id: ' .
								$result['transaction_id'] : ''),
					]
				);
				$data['payment_status'] = isset($result['errmsg']) ? $result['errmsg'] : 'iTunes Verify fail';
			}
		}

		return $data;
	}

	/**
	 * Получение данных c Itunes
	 *
	 * @param string $receipt
	 * @return array
	 */
	public static function getReceiptData($receipt)
	{
		if (empty($receipt)) {
			FastLog::add('payment_error', ['msg' => "getItunesReceiptData. Verifying data undefined"]);

			return ['fail' => true, 'errmsg' => 'Verifying data undefined'];
		}

		// determine which endpoint to use for verifying the receipt
		$endpoint = 'https://buy.itunes.apple.com/verifyReceipt';

		// build the post data
		$postData = json_encode(['receipt-data' => $receipt]);

		// create the cURL request
		$ch = curl_init($endpoint);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

		// execute the cURL request and fetch response data
		$response = curl_exec($ch);
		$errno = curl_errno($ch);
		$errmsg = curl_error($ch);
		curl_close($ch);

		// ensure the request succeeded
		if ($errno != 0) {
			FastLog::add('payment_error', ['msg' => "getItunesReceiptData. Запрос к Itunes был неудачным."]);

			return ['fail' => true, 'errmsg' => $errmsg, 'errno' => $errno];
		}

		// parse the response data
		$data = json_decode($response, true);

		// ensure response data was a valid JSON string
		if (!is_object($data)) {
			FastLog::add('payment_error', ['msg' => "getItunesReceiptData. Не корректный ответ."]);

			return ['fail' => true, 'errmsg' => 'Invalid response data'];
		}

		// ensure the expected data is present
		if (!isset($data['status'])) {
			FastLog::add('payment_error', ['msg' => "getItunesReceiptData. Не корректная покупка: Статус не найден"]);

			return ['fail' => true, 'errmsg' => 'Invalid receipt: udefined status'];
		}

		// Если статус ответа 21007 то верифицировать в любом случае надо через сандбокс
		if ($data['status'] == 21007) {

			$endpoint = 'https://sandbox.itunes.apple.com/verifyReceipt';

			// create the cURL request
			$ch = curl_init($endpoint);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

			// execute the cURL request and fetch response data
			$response = curl_exec($ch);
			$errno = curl_errno($ch);
			$errmsg = curl_error($ch);
			curl_close($ch);

			// ensure the request succeeded
			if ($errno != 0) {
				FastLog::add('payment_error', ['msg' => "getItunesReceiptData. Запрос к Itunes был неудачным."]);

				return ['fail' => true, 'errmsg' => $errmsg, 'errno' => $errno];
			}

			// parse the response data
			$data = json_decode($response, true);
		}

		// ensure response data was a valid JSON string
		if (!is_object($data)) {
			FastLog::add('payment_error', ['msg' => "getItunesReceiptData. Не корректный ответ."]);

			return ['fail' => true, 'errmsg' => 'Invalid response data'];
		}

		// ensure the expected data is present
		if (!isset($data['status'])) {
			FastLog::add('payment_error', ['msg' => "getItunesReceiptData. Не корректная покупка: Статус не найден"]);

			return ['fail' => true, 'errmsg' => 'Invalid receipt: udefined status'];
		}

		if ($data['status'] != 0) {
			FastLog::add('payment_error', ['msg' => "getItunesReceiptData. Не корректная покупка!"]);

			return ['fail' => true, 'status' => $data['status'], 'errmsg' => 'Invalid receipt'];
		}

		$result = [];
		foreach ($data['receipt']['in_app'] as $key => $var) {
			$result[] = [
				'status'         => $data['status'],
				'product_id'     => $var['product_id'],
				'transaction_id' => $var['transaction_id'],
				'purchase_date'  => $var['purchase_date_ms'],
				'endpoint'       => $endpoint,
				'receipt_data'   => $receipt,
				'bid'            => $data['receipt']['bundle_id'],
			];
		}

		// build the result array with the returned data
		return $result;
	}
}