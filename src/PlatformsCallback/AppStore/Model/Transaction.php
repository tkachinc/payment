<?php
namespace TkachInc\Payment\PlatformsCallback\AppStore\Model;

use TkachInc\Core\Database\MongoDB\ObjectModel;

/**
 * Itunes Transaction
 *
 * @author Vasiliy Khvostik
 * @property string $_id
 * @property string $user_id
 * @property string $transaction_id
 * @property int $time
 * @property string $endpoint
 * @property array $itunes_response
 */
class Transaction extends ObjectModel
{
	protected static $_collection = 'itunesTransaction';
	protected static $_pk = '_id';
	protected static $_sort = ['time' => -1];
	protected static $_indexes = [
		[
			'name' => 'time_-1',
			'keys' => ['time' => -1],
		],
		[
			'name' => 'transaction_id_1',
			'keys' => ['transaction_id' => 1],
		],
	];
	protected static $_fieldsDefault = [
		'_id'             => '',
		'user_id'         => '',
		'transaction_id'  => '',
		'time'            => 0,
		'endpoint'        => '',
		'itunes_response' => [],
	];
	protected static $_fieldsValidate = [
		'_id'             => self::TYPE_MONGO_ID,
		'user_id'         => self::TYPE_STRING,
		'transaction_id'  => self::TYPE_STRING,
		'time'            => self::TYPE_TIMESTAMP,
		'endpoint'        => self::TYPE_STRING,
		'itunes_response' => self::TYPE_JSON,
	];

	// ОБЕЗАТЕЛЬНЫЕ ПОЛЯ
	protected static $_isCacheOn = true;
	protected static $_updateMethod = self::UPDATE_METHOD_SET;
}
