<?php
namespace TkachInc\Payment\PlatformsCallback\AppStore;

use TkachInc\Core\Config\Config;
use TkachInc\Engine\Services\Request\Request;
use TkachInc\Payment\PlatformsCallback\AppStore\Service\ITunes;
use TkachInc\Payment\PlatformsCallback\BaseCallback;

/**
 * Class CallbackCommand
 *
 * @package TkachInc\Payment\Platform\iOS
 */
class CallbackCommand extends BaseCallback
{
	public function execute()
	{
		$receipt = (string)Request::getGetOrPost('receipt', '');
		$userId = (string)Request::getGetOrPost('userId', '');
		$transactionId = (string)Request::getGetOrPost('transactionId', '');

		$itunes_bundel_id = (string)Config::getInstance()->get(['itunes_bundel_id']);

		$itune = new ITunes($this->paymentEvents, $this->params, $this->netId);

		return $itune->verifyPurchase($receipt, $userId, $transactionId, $itunes_bundel_id);
	}
}