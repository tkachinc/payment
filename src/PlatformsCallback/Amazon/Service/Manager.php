<?php
namespace TkachInc\Payment\PlatformsCallback\Amazon\Service;

use TkachInc\Core\Log\FastLog;
use TkachInc\Payment\PlatformsCallback\BaseService;

/**
 * Class Manager
 *
 * @package TkachInc\Payment\PlatformsCallback\Amazon\Service
 */
class Manager extends BaseService
{
	/**
	 * Api Амазона
	 *
	 * @var string
	 */
	private static $amazonHost = "https://appstore-sdk.amazon.com";

	/**
	 * Верификация покупки
	 *
	 * @param string $amazonSecretKey - секретный ключ
	 * @param string $purchaseToken - токен, который получает клиент при проведении платежа
	 * @param string $userId - id пользователя в приложении
	 * @param string $amazonUserId - id пользователя в Amazon
	 * @param string $testHost - тестовый сервер для проверки платежей
	 * @param array $options - дополнительные параметры
	 * @return array
	 */
	public function verifyPurchase(...$params)
	{
		list($amazonSecretKey, $purchaseToken, $userId, $amazonUserId, $testHost, $options) = $params;

		$data = ['result' => false];

		$response = static::getAmazonData($amazonSecretKey, $purchaseToken, $amazonUserId, $testHost);

		if (isset($response['sku']) &&
			isset($response["purchaseToken"]) &&
			$response["purchaseToken"] == $purchaseToken
		) {
			try {
				$productId = (string)$response['sku'];
				$transactionId = hash('sha1', $purchaseToken);

				$result = $this->paymentEvent($productId, $userId, $transactionId, $options);
				if ($result) {
					$data['result'] = true;
					$data['payment_status'] = 'complete';
				} else {
					FastLog::add(
						'payment_error',
						['msg' => 'Payment event was not processed. PurchaseToken: ' . $response['purchaseToken']]
					);
					$data['payment_status'] = 'Payment event was not processed';
				}

			} catch (\Exception $e) {
				FastLog::add(
					'payment_error',
					['msg' => 'Exception: ' . $e->getMessage() . ' on ' . $e->getFile() . ':' . $e->getLine()]
				);
				$data['payment_status'] = 'Payment processing failed';
			}
		} else {
			FastLog::add('payment_error', ['msg' => 'Validate failed. Response: ' . json_encode($response)]);
			$data['payment_status'] = 'Validate failed';
		}

		return $data;
	}

	/**
	 * @param $amazonSecretKey
	 * @param $purchaseToken
	 * @param $amazonUserId
	 * @param $testHost
	 * @return mixed|string
	 */
	public static function getAmazonData($amazonSecretKey, $purchaseToken, $amazonUserId, $testHost)
	{
		$host = !empty($testHost) ? $testHost : static::$amazonHost;
		$url = $host .
			'/version/2.0/verify/developer/' .
			$amazonSecretKey .
			'/user/' .
			$amazonUserId .
			'/purchaseToken/' .
			$purchaseToken;
		try {
			$curl = curl_init($url);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			// получаем ответ
			$responseAmazon = json_decode(curl_exec($curl), true);
			curl_close($curl);
		} catch (\Exception $e) {
			$responseAmazon = $e->getMessage();
		}

		return $responseAmazon;
	}

}
