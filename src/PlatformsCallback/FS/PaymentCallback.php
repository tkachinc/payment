<?php
namespace TkachInc\Payment\PlatformsCallback\FS;

use TkachInc\Payment\PlatformsCallback\BaseCallback;

/**
 * @author Maxim Tkach <gollariel@gmail.com>
 */
class PaymentCallback extends BaseCallback
{
	/**
	 * @param $params
	 * @param $appSecretKey
	 * @return bool|string
	 */
	public static function checkSig($params, $appSecretKey)
	{

		if (isset($params['sig'])) {
			unset($params['sig']);
			ksort($params);
			$sigStr = '';
			foreach ($params as $paramName => $paramVal) {
				$sigStr .= $paramName . '=' . $paramVal;
			}
			$sigStr .= $appSecretKey;

			return md5($sigStr);
		} else {
			return false;
		}
	}

	/**
	 * @return array
	 * @throws \Exception
	 */
	public function execute()
	{
		$appSecretKey = $this->params->getAppSecretKey();

		if (!isset($_POST['type']) || $_POST['type'] != 1) {
			throw new \Exception('Error params');
		}

		$params = [
			'type'          => isset($_POST['type']) ? (string)$_POST['type'] : '',
			'appUid'        => isset($_POST['appUid']) ? (string)$_POST['appUid'] : '',
			'transactionId' => isset($_POST['transactionId']) ? (string)$_POST['transactionId'] : '',
			'itemId'        => isset($_POST['itemId']) ? (string)$_POST['itemId'] : '',
			'amount'        => isset($_POST['amount']) ? (string)$_POST['amount'] : '',
			'userId'        => isset($_POST['userId']) ? (string)$_POST['userId'] : '',
			'priceFmCents'  => isset($_POST['priceFmCents']) ? (string)$_POST['priceFmCents'] : '',
			'receiverId'    => isset($_POST['receiverId']) ? (string)$_POST['receiverId'] : '',
			'forwardData'   => isset($_POST['forwardData']) ? (string)$_POST['forwardData'] : '',
			'isDebug'       => isset($_POST['isDebug']) ? (string)$_POST['isDebug'] : '',
			'sig'           => isset($_POST['sig']) ? (string)$_POST['sig'] : '',
		];

		$sig = (string)$params['sig'];
		$validSig = (string)static::checkSig($params, $appSecretKey);

		$response = ['result' => 0, 'isTemp' => 0];

		if ($validSig === $sig) /* ВАЛИДАЦИЯ АУТЕНТИЧНОСТИ ПЛАТЕЖА */ {
			$price = (float)$params['priceFmCents'] / 100;
			$serviceId = (int)$params['itemId'];
			$userId = (string)$params['receiverId'];
			$transactionId = (string)$params['transactionId'];

			try {
				$result = $this->paymentEvent(
					$serviceId,
					$userId,
					$transactionId,
					['price' => $price, 'additionParams' => $params['forwardData']]
				);

				if ($result) {
					return ['result' => 1, 'transactionId' => $transactionId];
				} else {
					return $response;
				}
			} catch (\Exception $e) {
				return $response;
			}
		} else {
			return $response;
		}
	}
}